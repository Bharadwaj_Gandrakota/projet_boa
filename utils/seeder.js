const Datastore = require('nedb')        // set up a temporary (in memory) database 
const accountData = require('../data/account.json')  // read in data file 
const transactionData = require('../data/transaction.json')


// inject Express app to configure it - EVERYTHING in through argument list

module.exports = (app) => {

  console.log('START data seeder.')
  const db = {}  // empty object to hold all collections
  const transactions = {}

  db.accounts = new Datastore()  // new object property 
  db.accounts.loadDatabase()     // call the loadDatabase method

  transactions.dat = new Datastore()  
  transactions.dat.loadDatabase()



  //db.testers = new Datastore()  // new object property 
  //db.testers.loadDatabase()  

  // insert the sample data into our datastore
  //db.testers.insert(testerData)
  db.accounts.insert(accountData);

  transactions.dat.insert(transactionData);

  // initialize app.locals (these objects are available to the controllers)
  app.locals.accounts = db.accounts.find(accountData)
  app.locals.transactions = transactions.dat.find(transactionData)
  //app.locals.testers = db.developers.find(testerData)
  console.log(`${app.locals.accounts.query.length} accounts seeded`)

  console.log(`${app.locals.transactions.query.length} transactions seeded`)
   
  console.log('END Data Seeder. Sample data read and verified.')
}
